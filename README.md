# Documentazione

Repository contenente la documentazione delle esperienze fatte sperimentando le varie tecnologie che ci sono

## Docker e Swarm
Migliori tutorial finora:
- https://medium.com/google-cloud/docker-swarm-on-google-cloud-platform-c9925bd7863c
- https://www.youtube.com/watch?v=3-7gZS4ePak
- https://youtu.be/JDBq2JvzQOY

## Spring e Hibernate
Video completi su come funziona il framework, spiegazione anche dei design pattern utilizzati:
- https://youtu.be/35EQXmHKZYs
- https://youtu.be/JR7-EdxDSf0

### Metodo migliore finora di implementazione
- https://youtu.be/avvrsnYFXIE (Parte 1)
- https://www.youtube.com/watch?v=jCalDxGthoc (Parte 2)

## Django
Esplorazione del framework e della sua integrazione con React e Postgres.
Un video introduttivo:
- https://youtu.be/D6esTdOLXh4

Ideale per creare un'applicazione in pochissimo tempo dimostrazione delle potenzialitą:
Una dashboard admin create in 3 minuti contati:
- https://youtu.be/D6esTdOLXh4?t=3374

Esplorando questo framework sembra sia molto valido per la nostra situazione l'integrazione con servizi di terze parti REACT e anche le API REST sono davvero la ciliegina:
- https://youtu.be/Uyei2iDA4Hs

## Opinioni su Django vs Hibernate
- https://www.quora.com/Shall-I-choose-Spring-Boot-or-Django
- https://hackernoon.com/10-popular-websites-built-with-django-906cc310aa0a

# GraphQL / Apollo / React (Stack potentissima)
- https://youtu.be/kXH2dbnHYA0

## ORM e connessione a Postgres
- https://blog.logrocket.com/why-you-should-avoid-orms-with-examples-in-node-js-e0baab73fa5/
